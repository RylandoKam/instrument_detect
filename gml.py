import numpy as np
from numpy import linalg as la
from scipy import special


def gml(piano, guitar, trumpet, feature_mat, test_vec):
    w_test = np.dot(feature_mat, test_vec)


    mu1 = np.mean(piano, axis=1, keepdims=True)
    mu2 = np.mean(guitar, axis=1, keepdims=True)
    mu3 = np.mean(trumpet, axis=1, keepdims=True)


    sig1 = np.cov(piano)
    sig2 = np.cov(guitar)
    sig3 = np.cov(trumpet)

    dim = w_test.shape[0]
    a = w_test-mu1
    b = w_test-mu2
    c = w_test-mu3
    exp1 = (-0.5)*a.T*la.inv(sig1)*a
    prob1 = (1/np.sqrt((2*np.pi**dim*la.det(sig1))))*special.expit(exp1) #prop piano

    exp2 = (-0.5)*b.T*la.inv(sig1)*b
    prob2 = (1/np.sqrt((2*np.pi**dim*la.det(sig2))))*special.expit(exp2) #prob guitar

    exp3 = (-0.5)*c.T*la.inv(sig3)*c
    prob3 = (1/np.sqrt((2*np.pi**dim*la.det(sig3))))*special.expit(exp3) #prob Trumpet

    #print(sig1)
    #print("Piano probability: ", prob1[0][0])
    #print("\nGuitar probability: ", prob2[0][0])
    #print("\nTrumpet probability: ", prob3[0][0])

    probArray = [prob1[0][0], prob2[0][0], prob3[0][0]]
    maxProb = max(probArray)
    #print("max prob", maxProb)

    if maxProb == prob1[0][0]:
        return (0,prob1[0][0],prob2[0][0],prob3[0][0])
    if maxProb == prob2[0][0]:
        return (1,prob1[0][0],prob2[0][0],prob3[0][0])
    return (2,prob1[0][0],prob2[0][0],prob3[0][0])
