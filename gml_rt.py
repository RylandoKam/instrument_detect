import numpy as np
from numpy import linalg as la

#def gml(pos,neg,w_mat,d_test):
def gml(inv_sigp,dsigp,mu1,inv_sign,dsign,mu2,w_mat,d_test):
    w_test = np.dot(w_mat,d_test)
    # sig1 = np.cov(pos)
    # sig2 = np.cov(neg)
    # mu1 = np.mean(pos,axis=1)
    # mu2 = np.mean(neg,axis=1)
    dim = w_test.shape[0]
    a = w_test-mu1
    b = w_test-mu2
    # exp1 = (-0.5)*a.T*la.inv(sig1)*a
    exp1 = (-0.5)*a.T*inv_sigp*a
    # prob1 = np.exp(exp1)/np.sqrt((2*np.pi)**dim*la.det(sig1))
    prob1 = np.exp(exp1)/np.sqrt((2*np.pi)**dim*dsigp)
    # exp2 = (-0.5)*b.T*la.inv(sig1)*b
    exp2 = (-0.5)*b.T*inv_sign*b
    # prob2 = np.exp(exp2)/np.sqrt((2*np.pi)**dim*la.det(sig2))
    prob2 = np.exp(exp2)/np.sqrt((2*np.pi)**dim*dsign)
    return prob1[0][0] >= prob2[0][0]
