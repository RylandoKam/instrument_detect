import sounddevice as sd
from scipy.io.wavfile import write

def record():
    fs = 48000  # Sample rate
    seconds = 8  # Duration of recording
    myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=1,device=12,blocking=True)
    sd.wait()  # Wait until recording is finished
    write('recorded.wav', fs, myrecording)