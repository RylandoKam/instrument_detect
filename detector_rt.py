import numpy as np
from numpy import linalg as la
from gml import gml
import spectro as sp
from matplotlib import pyplot as plt
from recorder import record
from time import sleep

# def find(w1,w2,w3,feat,spect):
#     insts = ['Piano','Guitar','Trumpet']
#     vec = np.reshape(spect, (64*64,1))
#     i_type = gml(w1, w2, w3, feat, vec)
#     return insts[i_type]

def process(norm):
    insts = ['Piano','Guitar','Trumpet','Unsure']
    spect = sp.spect_samp('recorded.wav')
    feat = np.load('feat_mat.npy')
    w1 = np.load('piano_weight.npy')
    w2 = np.load('guitar_weight.npy')
    w3 = np.load('trumpet_weight.npy')
    vec = np.reshape(spect, (64*64,1))
    i_type = gml(w1, w2,w3, feat, vec)
    a = np.array(i_type[1:4])
    b = np.array(norm[1:4])
    diff = 0.5*(a-b)/(a+b)
    if len(np.unique(diff)) == len(diff):
        for k in range(diff.size):
            if diff.max()==diff[k:k+1]:
                inst = k
    else:
        inst = 3
    print(a)
    print(b)
    print(diff)
    print(insts[inst], "Detected")

def norm():
    record()
    spect = sp.spect_samp('recorded.wav')
    feat = np.load('feat_mat.npy')
    w1 = np.load('piano_weight.npy')
    w2 = np.load('guitar_weight.npy')
    w3 = np.load('trumpet_weight.npy')
    vec = np.reshape(spect, (64*64,1))
    i_type = gml(w1, w2, w3, feat, vec)
    return i_type

def main():
    while True:
        key = input('Type "r" and enter to record:')
        if key == 'r':
            print('Starting Normalization...')
            n_vec = norm()
            print(chr(27) + "[2J")
            print('Recording in...')
            print('3')
            sleep(1)
            print('2')
            sleep(1)
            print('1')
            sleep(1)
            print('Recording started...')
            record()
            print('Recording complete')
            process(n_vec)


if __name__ == '__main__':
    main()