import glob
import numpy as np
from gml import gml
import spectro as sp

def find(w1,w2,w3,feat,spect):
    insts = ['Piano', 'Guitar', 'Trumpet']
    vec = np.reshape(spect, (64*64,1))
    i_type = gml(w1, w2, w3, feat, vec)
    return insts[i_type]

def main():
    #change this to sample after building recording code
    #spect = sp.spect_data('PianoData/piano40.wav')
    #spect = sp.spect_data('GuitarData/Processed/guitar10.wav')
    #spect = sp.spect_data('TrumpetData/trumpet30.wav')
    pianoNotes = glob.glob('PianoData/Processed/*.wav')
    guitarNotes = glob.glob('GuitarData/Processed/*.wav')
    trumpetNotes = glob.glob('TrumpetData/Processed/*.wav')
    feat = np.load('feat_mat.npy')
    w1 = np.load('piano_weight.npy')
    w2 = np.load('guitar_weight.npy')
    w3 = np.load('trumpet_weight.npy')

    correct = 0
    incorrect = 0

    for k in range(len(pianoNotes)-1):
        spect = sp.spect_data(pianoNotes[k])
        detected = find(w1, w2, w3, feat, spect)
        if (detected == 'Piano'):
            correct += 1
            print('correct!')
        else:
            incorrect += 1
    for k in range(len(guitarNotes)-1):
        spect = sp.spect_data(guitarNotes[k])
        i_type = find(w1, w2, w3, feat, spect)
        detected = find(w1, w2, w3, feat, spect)
        if (detected == 'Piano' or detected == 'Guitar'):
            correct += 1
            print('correct!')
        else:
            incorrect += 1
    for k in range(len(trumpetNotes)-1):
        spect = sp.spect_data(trumpetNotes[k])
        detected = find(w1, w2, w3, feat, spect)
        if (detected == 'Trumpet'):
            correct += 1
            print('correct!')
        else:
            incorrect += 1
    print("Correct: ", correct)
    print("Incorrect: ",incorrect)


if __name__ == '__main__':
    main()
