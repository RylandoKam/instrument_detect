import numpy as np
#from numpy import linalg as la
from scipy import linalg as la

def pca(in_mat, dim):
    '''
    Solution modified from https://stackoverflow.com/users/66549/doug
    '''
    mu = in_mat.mean(axis=0, keepdims=True)
    std = in_mat.std(axis=0, keepdims=True)
    in_mat = (in_mat - mu) / std
    sig = np.cov(in_mat, rowvar=False)
    eigh_val, eigh_vec = la.eigh(sig)
    idx = np.argsort(eigh_val)[::-1]
    eigh_val = eigh_val[idx]
    eigh_val = np.identity(eigh_val.size)
    eigh_val = eigh_val[:dim, :dim]
    eigh_vec = eigh_vec[:,idx]
    eigh_vec = eigh_vec[:,:dim]
    features = np.dot(la.inv(np.sqrt(eigh_val)), eigh_vec.T)

    return features
