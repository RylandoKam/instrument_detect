# Displays a Spectrogram of the given audio file
# Based off of code from:
# https://towardsdatascience.com/extract-features-of-music-75a3f9bc265d
# and
# https://stackoverflow.com/questions/56719138/how-can-i-save-a-librosa-spectrogram-plot-as-a-specific-sized-image/57204349#57204349
import librosa
import librosa.display
import numpy as np
import cv2 as cv

def scale(data):
    data_norm = (data - data.min())/(data.max()-data.min())
    return data_norm*255


def spect_data(path):
    sig,samp_rate = librosa.load(path)
    mels = librosa.feature.melspectrogram(y=sig,sr=samp_rate)
    mels = np.log(mels+1E-9)
    img = scale(mels).astype(np.uint8)
    img = np.flip(img,axis=0)
    img = img[0:127,0:127]
    img = cv.resize(img,(64,64))
    return img

def spect_samp(path):
    dsamp_rate = 44.1E3
    sig,samp_rate = librosa.load(path)
    sig = librosa.resample(sig, samp_rate, dsamp_rate)
    mels = librosa.feature.melspectrogram(y=sig,sr=dsamp_rate)
    mels = np.log(mels+1E-9)
    img = scale(mels).astype(np.uint8)
    img = np.flip(img,axis=0)
    img = cv.resize(img,(64,64))
    return img
