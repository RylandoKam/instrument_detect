import numpy as np
import spectro as sp
import glob
import matplotlib.pyplot as plt

class mat_maker:
    def __init__(self):

        self.guitar_names = glob.glob('GuitarData/Processed/*.wav')
        self.piano_names = glob.glob('PianoData/*.wav') + glob.glob('PianoData2/*.wav')
        self.trumpet_names = glob.glob('TrumpetData/*.wav')
        
    def mat_build(self,name_list):
        mat = np.empty((64*64,1))
        for k in range(len(name_list)-1):
            gram = sp.spect_data(name_list[k])
            vec = np.reshape(gram, (gram.shape[0]*gram.shape[1], 1))
            mat = np.hstack((mat, vec))
        return mat
    
    def build(self):
        guitar_mat = self.mat_build(self.guitar_names)
        piano_mat = self.mat_build(self.piano_names)
        trumpet_mat = self.mat_build(self.trumpet_names)
        np.save('in_guitar.npy', guitar_mat)
        np.save('in_piano.npy', piano_mat)
        np.save('in_trumpet.npy', trumpet_mat)
