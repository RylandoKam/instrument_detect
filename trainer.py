import numpy as np
from builder import mat_maker
from pca import pca


def main():
    #build_data = mat_maker() #can comment out after input matrices
    #build_data.build()       #are first created (takes a lot of time)
    in_piano = np.load('in_piano.npy')
    in_guitar = np.load('in_guitar.npy')
    in_trumpet = np.load('in_trumpet.npy')

    in_mat = np.hstack((in_trumpet, in_piano)) #order of input matrices affects accuracy?
    in_mat = np.hstack((in_mat, in_guitar)) #trumpet-piano-guitar gave accuracy of 231/278
    features = pca(in_mat.T, 5)             #when reusing testing data (see detector.py)
    piano_weight = np.dot(features, in_piano)
    guitar_weight = np.dot(features, in_guitar)
    trumpet_weight = np.dot(features, in_trumpet)

    np.save('feat_mat.npy', features)
    np.save('piano_weight.npy', piano_weight)
    np.save('guitar_weight.npy', guitar_weight)
    np.save('trumpet_weight.npy', trumpet_weight)

if __name__ == '__main__':
    main()
    print('Training Complete')
